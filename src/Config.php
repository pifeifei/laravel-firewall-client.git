<?php


namespace Pff\FirewallClient;

use Illuminate\Support\Arr;
use Illuminate\Support\Traits\Macroable;
use InvalidArgumentException;
use Pff\FirewallClient\Concerns\InternetProtocolTrait;

class Config
{
    use Macroable;
    use InternetProtocolTrait;

    protected $config = [
        'driver' => 'advfirewall',
        'prefix' => 'pfr_',
        'allow' => true,
        'ports' => '', // 22, 30:40
        'protocol' => 'TCP', // TCP | UDP | ICMP
        'ips' => [
//            '192.168.1.100',
//            '192.168.0.1/24',
//            '192.168.1.0-192.168.10.255', // iptables 不支持这种格式
        ],
        // 进、出: input output
    ];

    // advFirewall : profile=public|private|domain|any[,...]

    public function __construct($config)
    {
        $this->config = array_merge($this->config, $config);

        $this->setProtocol($this->getProtocol());
        $this->setIps($this->getIps());
    }

    /**
     * @return string
     */
    public function getDriver(): string
    {
        return Arr::get($this->config, 'driver');
    }

    /**
     * @param string $driver
     * @return Config
     */
    public function setDriver(string $driver): Config
    {
        Arr::set($this->config, 'driver', strtolower($driver));
        return $this;
    }

    /**
     * @return string
     */
    public function getPrefix(): string
    {
        return Arr::get($this->config, 'prefix', 'pfn_');
    }

    /**
     * @param string $prefix
     * @return Config
     */
    public function setPrefix(string $prefix): Config
    {
        Arr::set($this->config, 'prefix', $prefix);
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return Arr::get($this->config, 'name');
    }

    /**
     *
     * @param string $name
     * @return Config
     */
    public function setName(string $name): Config
    {
        Arr::set($this->config, 'name', $name);
        return $this;
    }

    /**
     * @return string
     */
    public function getService(): string
    {
        return $this->getPrefix() . $this->getShortService();
    }

    /**
     * @return string
     */
    public function getShortService(): string
    {
        return str_replace(
            ['{PORT}'],
            [$this->getPorts()],
            Arr::get($this->config, 'service', 'service_port_{PORT}')
        );
//        return sprintf('service_port_%s', $this->getPorts());
    }

    /**
     * @return bool
     */
    public function getAllow(): bool
    {
        return Arr::get($this->config, 'allow');
    }

    /**
     * @param bool $allow
     * @return Config
     */
    public function setAllow(bool $allow = true): Config
    {
        Arr::set($this->config, 'allow', (bool)$allow);
        return $this;
    }

    /**
     * @return int|string
     */
    public function getPorts()
    {
        return Arr::get($this->config, 'ports');
    }

    /**
     * @param int|string $ports
     * @return Config
     */
    public function setPorts($ports): Config
    {
        Arr::set($this->config, 'ports', $ports);
        return $this;
    }

    /**
     * @param string $ip
     * @return $this
     */
    public function addIp(string $ip): Config
    {
        array_push($this->config['ips'], $this->filterIp($ip));
        return $this;
    }
    /**
     * @return string|string[]
     */
    public function getIps()
    {
        return Arr::get($this->config, 'ips');
    }

    /**
     * @param string[] $ips
     * @return Config
     */
    public function setIps(array $ips): Config
    {
        Arr::set($this->config, 'ips', $this->filterIps($ips));
        return $this;
    }

    /**
     * 过滤ip
     * @param string $ip
     * @return string
     */
    protected function filterIp(string $ip): string
    {
        if (strpos($ip, ' ')) {
            $ip = str_replace(' ', '', $ip);
        }

        if (filter_var($ip, FILTER_VALIDATE_IP)) {
            return $ip;
        }

        if (strpos($ip, '-')) {
            $ips = explode('-', $ip, 2);
            if (filter_var($ips[0], FILTER_VALIDATE_IP) && filter_var($ips[0], FILTER_VALIDATE_IP)) {
                return $ip;
            }

            throw new InvalidArgumentException('$ips parameter format error. sample: 192.168.1.100, 192.168.0.1/24, 192.168.1.0-192.168.10.255');
        }

        if (strpos($ip, '/')) {
            $ips = explode('/', $ip, 2);
            if (filter_var($ips[0], FILTER_VALIDATE_IP) && intval($ips[1]) <= 32) {
                return $this->netmask($ips[0], intval($ips[1]));
            }
        }

        throw new InvalidArgumentException('$ips parameter format error. sample: 192.168.1.100, 192.168.0.1/24, 192.168.1.0-192.168.10.255');
    }

    /**
     * @param $ips
     * @return array
     */
    protected function filterIps($ips): array
    {
        $ips = Arr::wrap($ips);
        foreach ($ips as & $ip) {
            $ip = $this->filterIp($ip);
        }

        return $ips;
    }


    /**
     * @return string
     */
    public function getProtocol(): string
    {
        return Arr::get($this->config, 'protocol');
    }

    /**
     * @param string $protocol
     * @return Config
     */
    public function setProtocol(string $protocol): Config
    {
        Arr::set($this->config, 'protocol', $this->filterProtocol($protocol));
        return $this;
    }

    /**
     * @param string $protocol
     * @return string
     */
    protected function filterProtocol(string $protocol): string
    {
        if (! is_string($protocol)) {
            throw new InvalidArgumentException('The $protocol can only be string.');
        }

        if (! in_array($protocol = strtoupper($protocol), ['TCP', 'UDP', 'ICMP'])) {
            // protocol: udplite, icmpv6,esp, ah, sctp, mh
            throw new InvalidArgumentException('The $protocol can only be TCP, UDP, ICMP.');
        }

        return $protocol;
    }

    public function toArray(): array
    {
        return $this->config;
    }
}
