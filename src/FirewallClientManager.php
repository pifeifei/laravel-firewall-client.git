<?php


namespace Pff\FirewallClient;


use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Arr;
use InvalidArgumentException;

class FirewallClientManager
{
    /**
     * The application instance.
     *
     * @var Application
     */
    protected $app;

    /**
     * The FirewallClientFactory instances.
     *
     * @var array
     */
    protected $clients = [];

    /**
     * The name of the default driver.
     *
     * @var string
     */
    protected $driver;

    /**
     * The database connection factory instance.
     *
     * @var FirewallClientFactory
     */
    protected $factory;

    /**
     * Create a new Redis manager instance.
     *
     * @param  Application  $app
     * @return void
     */
    public function __construct(Application $app, FirewallClientFactory $factory)
    {
        $this->app = $app;
        $this->factory = $factory;
    }

    /**
     * Get a firewall client instance.
     *
     * @param string|null $name
     * @return mixed
     */
    public function client(string $name = null)
    {

        $client = $this->parseClientName($name);

        $name = $name ?: $client;

        // If we haven't created this connection, we'll create it based on the config
        // provided in the application. Once we've created the connections we will
        // set the "fetch mode" for PDO which determines the query return types.
        if (! isset($this->clients[$name])) {
            $this->clients[$name] = $this->configure($this->makeClient($client));
        }

        return $this->clients[$name];
    }

    /** 99
     * Parse the connection into an array of the name and read / write type.
     *
     * @param  string  $name
     * @return string
     */
    protected function parseClientName($name)
    {
        $name = $name ?: $this->getDefaultClient();

        return $name;
    }

    /** 113
     * Make the database connection instance.
     *
     * @param  string  $name
     * @return Contracts\Driver
     */
    protected function makeClient($name)
    {
        $config = $this->configuration($name);

        return $this->factory->make($config, $name);
    }

    /** 142
     * Get the configuration for a client.
     *
     * @param  string  $name
     * @return array
     *
     * @throws InvalidArgumentException
     */
    protected function configuration($name)
    {
        $name = $name ?: $this->getDefaultClient();

        // To get the firewall client configuration
        $connections = $this->app['config']['firewall-client.firewalls'];

        if (is_null($config = Arr::get($connections, $name))) {
            throw new InvalidArgumentException("FirewallClientFactory client [{$name}] not configured.");
        }

        return $config;
    }

    /** 159
     * Prepare the firewall client instance.
     *
     * @param  Contracts\Driver  $driver
     * @return Contracts\Driver
     */
    protected function configure(Contracts\Driver $driver)
    {
        // 可以加 events

        return $driver;
    }

    /** 294
     * Get the default connection name.
     *
     * @return string
     */
    public function getDefaultClient()
    {
        return $this->app['config']['firewall-client.default'];
    }

    /** 305
     * Set the default connection name.
     *
     * @param  string  $name
     * @return void
     */
    public function setDefaultClient(string $name)
    {
        $this->app['config']['firewall-client.default'] = $name;
    }

    /**
     * Dynamically pass methods to the default client.
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        return $this->client()->$method(...$parameters);
    }
}
