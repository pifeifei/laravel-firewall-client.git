<?php

return [
    'default' => env('FIREWALL_CLIENT', 'advfirewall'),

    'firewalls' => [
        'advfirewall' => [
            'driver' => 'advfirewall',
            'prefix' => env('FC_NAME_PREFIX', 'pfn_'),// pfn: pff firewall name
            'service' => env('FC_SERVICE', 'service_port_{PORT}'),
            'ports' => env('FC_PORT', 80),
            'allow' => env('FC_ALLOW', true),
            'protocol' => env('FC_PROTOCOL', 'TCP'),
            'ips' => explode(',', env('FC_IPS', '127.0.0.1')),
        ],
        'iptables' => [
            'driver' => 'iptables',
            'ports' => env('FC_PORT', 80),
            'allow' => env('FC_ALLOW', true),
            'protocol' => env('FC_PROTOCOL', 'TCP'),
            'ips' => explode(',', env('FC_IPS', '127.0.0.1')),
        ],
        'firewall' => [
            'driver' => 'firewall',
            'prefix' => env('FC_NAME_PREFIX', 'pfn_'),// pfn: pff firewall name
            'service' => env('FC_SERVICE', 'service_port_{PORT}'),
            'ports' => env('FC_PORT', 80),
            'allow' => env('FC_ALLOW', true),
            'protocol' => env('FC_PROTOCOL', 'TCP'),
            'ips' => explode(',', env('FC_IPS', '127.0.0.1')),
        ],
//        'service' => [
//            'driver' => 'firewall',
//            'ports' => env('FC_PORT', 80),
//            //...
//        ],
    ]
];
